package dick.android.dick

import android.app.Service
import android.content.Intent
import android.os.Handler
import android.os.IBinder
import java.util.*

class DickService : Service() {

    override fun onBind(intent: Intent?): IBinder {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Handler().postDelayed({
            val dialogIntent = Intent(this, MainActivity::class.java)
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(dialogIntent)
        }, getRandomNumberInRange(900000, 1800000).toLong())
        return Service.START_STICKY
    }

    private fun getRandomNumberInRange(min: Int, max: Int): Int {

        if (min >= max) {
            throw IllegalArgumentException("max must be greater than min")
        }

        val r = Random()
        return r.nextInt(max - min + 1) + min
    }
}